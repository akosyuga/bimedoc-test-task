from rest_framework import serializers

from bimedoc_api.serializers import NestedParserSerializer
from cloudsearch.services.exceptions import CloudSearchUnprocessableEntity
from organizations.serializers import OrganizationSerializer
from workers.serializers import WorkerSerializer


class CloudSearchEntitySerializer(NestedParserSerializer):
    """
    A generic representation of CloudSearchHitEntity.
    """

    id = serializers.CharField(max_length=100)
    activity_segment_name = serializers.CharField(max_length=100, required=False)
    activity_segment_code = serializers.CharField(max_length=6, required=False)
    bimedoc_client = serializers.BooleanField(required=False)
    building_number = serializers.IntegerField(required=False)
    city = serializers.CharField(max_length=50, required=False)
    email = serializers.EmailField(required=False)
    finess = serializers.CharField(max_length=50, required=False)
    finess_justice = serializers.CharField(max_length=50, required=False)
    first_name = serializers.CharField(max_length=50, required=False)
    gender = serializers.CharField(max_length=50, required=False)
    last_name = serializers.CharField(max_length=50, required=False)
    phone_number = serializers.CharField(max_length=50, required=False)
    postcode = serializers.CharField(max_length=50, required=False)
    profession_code = serializers.CharField(max_length=50, required=False)
    profession_name = serializers.CharField(max_length=50, required=False)
    registered_name = serializers.CharField(max_length=50, required=False)
    rpps_number = serializers.CharField(max_length=20, required=False)

    def create(self, validated_data):
        # Can be generalized into recursive parsing of configurable child serializers with additional operations
        # (e.g. linking workers and orgs). Skipped here since use-cases are not fully defined.
        cloudsearch_id = validated_data.pop('id', None)
        if cloudsearch_id:
            validated_data['cloudsearch_id'] = cloudsearch_id

        org_serializer = OrganizationSerializer(data=validated_data)
        worker_serializer = WorkerSerializer(data=validated_data)

        worker = self.create_nested_instance(worker_serializer)
        org = self.create_nested_instance(org_serializer)
        if worker and org:
            worker.organizations.add(org)

        data = {}
        if worker:
            data = {**WorkerSerializer(worker).data}
        if org:
            data = {**data, **OrganizationSerializer(org).data}

        if not data:
            raise CloudSearchUnprocessableEntity

        data['id'] = data['cloudsearch_id']
        del data['cloudsearch_id']
        return data
