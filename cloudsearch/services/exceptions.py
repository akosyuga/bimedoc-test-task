from rest_framework.exceptions import APIException
from rest_framework import status


class CloudSearchUnavailable(APIException):
    """
    Generic exception in case of CloudSearch 5xx.
    """

    status_code = status.HTTP_504_GATEWAY_TIMEOUT
    default_code = 'cloudsearch_unavailable'
    default_detail = 'Cloudsearch is unavailable.'


class CloudSearchFailedRequest(APIException):
    status_code = status.HTTP_504_GATEWAY_TIMEOUT
    default_code = 'cloudsearch_fail'
    default_detail = 'Cloudsearch request failed.'


class CloudSearchUnprocessableEntity(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_code = 'cloudsearch_unprocessable_entity'
    default_detail = "Selected entity couldn't be processed."

