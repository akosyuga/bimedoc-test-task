import requests
from requests import Response
from tenacity import retry, retry_if_exception_type, stop_after_attempt

from cloudsearch.services.exceptions import CloudSearchUnavailable, CloudSearchFailedRequest
from bimedoc_api.settings import DEFAULT_RETRIES_LIMIT


@retry(retry=retry_if_exception_type(CloudSearchUnavailable),
       reraise=True,
       stop=stop_after_attempt(DEFAULT_RETRIES_LIMIT))
def cloudsearch_search(search_term: str, **kwargs) -> Response:
    """
    Submits a request to Cloudsearch directory.
    In case of unavailability, retries for selected number of times.
    :param search_term: search term for "q" param
    :return: Cloudsearch response
    """
    optional_params = ['start', 'size']
    response = requests.get(url=f'https://6n1w6lwcmf.execute-api.eu-west-1.amazonaws.com/dev/',
                            params={'q': search_term, **{k: v for k, v in kwargs.items() if k in optional_params}})
    try:
        response.raise_for_status()
    except requests.HTTPError:
        if response.status_code >= 500:
            raise CloudSearchUnavailable
        else:
            raise CloudSearchFailedRequest(detail=response.json())

    return response
