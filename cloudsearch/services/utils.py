from rest_framework import status


def parse_status_code(code: int) -> int:
    if status.is_success(code):
        return status.HTTP_200_OK
    elif status.is_client_error(code):
        return status.HTTP_400_BAD_REQUEST
    elif status.is_server_error(code):
        return status.HTTP_504_GATEWAY_TIMEOUT


def already_exists(serializer_errors):
    unique_violations = []
    for k, v in serializer_errors.items():
        for error in v:
            if error.code == 'unique':
                unique_violations.append(k)
    return unique_violations


def get_entities(response_data) -> dict:
    """
    Parses CloudSearch response and yield each item
    :param response_data: Cloudsearch response in JSON
    :return: CloudSearch entities
    """
    for hit in response_data['hits']['hit']:
        yield hit['fields']
