from django.apps import AppConfig


class CloudsearchConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cloudsearch'
