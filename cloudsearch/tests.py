from unittest.mock import patch, Mock

from django.test import TestCase
from rest_framework import status

from rest_framework.test import APIClient

from bimedoc_api.settings import DEFAULT_RETRIES_LIMIT
from cloudsearch.serializers import CloudSearchEntitySerializer
from cloudsearch.services.exceptions import CloudSearchUnavailable
from cloudsearch.services.requests import cloudsearch_search
from cloudsearch.services.utils import parse_status_code
from organizations.models import Organization
from organizations.serializers import OrganizationSerializer
from workers.models import HealthCareWorker
from workers.serializers import WorkerSerializer


def get_valid_worker_data():
    return {"status": {"rid": "uraBkLYwgthACtQiaw==", "time-ms": 0}, "hits": {"found": 57, "start": 0, "hit": [
        {"id": "PER-10102681649-R10100000379477----60-",
         "fields": {"rpps_number": "10102681649", "city": "Nîmes", "activity_segment_code": "SA07",
                    "last_name": "ID BRAHIM", "activity_segment_name": "Cabinet individuel",
                    "registered_name": "CABINET ID BRAHIM NADIA", "bimedoc_client": "0", "gender": "MME",
                    "ident_tech_structure": "R10100000379477", "first_name": "NADIA", "building_number": "30 ",
                    "profession_name": "Infirmier", "street": " RUE MATISSE ", "postcode": "30900",
                    "profession_code": "60", "id": "PER-10102681649-R10100000379477----60-", "type_contact": "PER"}}]}}


def get_valid_organization_and_worker_data():
    return {"status": {"rid": "t5qdkbYwsthACtQiaw==", "time-ms": 0}, "hits": {"found": 1, "start": 0, "hit": [
        {"id": "PER-10000735125-F02001104501122011-020011045---21-D",
         "fields": {"rpps_number": "10000735125", "last_name": "STAUB-CUVILLER", "finess": "020011045",
                    "pharma_section_code": "D", "finess_justice": "020011037", "street": "Rue DE BELLEVUE ",
                    "postcode": "02100", "id": "PER-10000735125-F02001104501122011-020011045---21-D",
                    "city": "Saint-Quentin", "activity_segment_code": "SA33", "profession_name": "Pharmacien",
                    "registered_name": "PHARMACIE CARDON-NUYTTEN", "bimedoc_client": "0",
                    "ident_tech_structure": "F02001104501122011", "phone_number": "0323623858",
                    "activity_segment_name": "Pharmacie d'officine", "first_name": "ANNE-CECILE",
                    "profession_code": "21", "type_contact": "PER",
                    "libel_pharma_section": "Pharmacien adjoint, pharmacien remplaçant le titulaire, gérant après décès du titulaire, pharmacien multi-employeurs.",
                    "gender": "MME", "building_number": "10 "}}]}}

def get_valid_organization_data():
    return {"status": {"rid": "t5qdkbYwsthACtQiaw==", "time-ms": 0}, "hits": {"found": 1, "start": 0, "hit": [
        {"id": "PER-10000735125-F02001104501122011-020011045---21-D",
         "fields": {"rpps_number": "10000735125", "finess": "020011045",
                    "pharma_section_code": "D", "finess_justice": "020011037", "street": "Rue DE BELLEVUE ",
                    "postcode": "02100", "id": "PER-10000735125-F02001104501122011-020011045---21-D",
                    "city": "Saint-Quentin", "activity_segment_code": "SA33", "profession_name": "Pharmacien",
                    "registered_name": "PHARMACIE CARDON-NUYTTEN", "bimedoc_client": "0",
                    "ident_tech_structure": "F02001104501122011", "phone_number": "0323623858",
                    "activity_segment_name": "Pharmacie d'officine", "first_name": "ANNE-CECILE",
                    "profession_code": "21", "type_contact": "PER",
                    "libel_pharma_section": "Pharmacien adjoint, pharmacien remplaçant le titulaire, gérant après décès du titulaire, pharmacien multi-employeurs.",
                    "gender": "MME", "building_number": "10 "}}]}}


def get_invalid_organization_data():
    data = get_valid_organization_data()
    del data['hits']['hit'][0]['fields']['finess']
    del data['hits']['hit'][0]['fields']['registered_name']
    return data


def get_invalid_worker_data():
    data = get_valid_worker_data()
    del data['hits']['hit'][0]['fields']['last_name']
    del data['hits']['hit'][0]['fields']['first_name']
    del data['hits']['hit'][0]['fields']['profession_name']
    del data['hits']['hit'][0]['fields']['rpps_number']
    return data


def get_empty_response():
    return {"status": {"rid": "otyBg7YwmcNACtQiaw==", "time-ms": 7}, "hits": {"found": 0, "start": 0, "hit": []}}


def get_valid_search_term():
    return "Pharmacie d'officine"


def get_response_mock(status, content=None):
    response = Mock()
    response.status_code = status
    response.json = lambda: content
    return response


class CloudSearchFetchTest(TestCase):
    """ Test module for CloudSearch searches. """

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.URL = '/api/cloudsearch/search/'
        cls.client = APIClient()
        cls.valid_search_term = 'id'
        cls.invalid_search_term = '--------'

    @patch('cloudsearch.services.requests.requests.get')
    def test_cloudsearch_unavailable(self, handle_search_mock):
        handle_search_mock.return_value = get_response_mock(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            cloudsearch_search(self.valid_search_term)
        except Exception as e:
            self.assertEqual(cloudsearch_search.retry.statistics['attempt_number'], DEFAULT_RETRIES_LIMIT)
            self.assertTrue(isinstance(e, CloudSearchUnavailable))

    @patch('cloudsearch.services.requests.requests.get')
    def test_cloudsearch_bad_request(self, handle_search_mock):
        response_payload = {'message': 'Missing args'}
        handle_search_mock.return_value = get_response_mock(status=status.HTTP_400_BAD_REQUEST,
                                                            content=response_payload)
        response = cloudsearch_search(self.valid_search_term)
        self.assertEqual(cloudsearch_search.retry.statistics['attempt_number'], 1)
        self.assertEqual(parse_status_code(response.status_code), status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), response_payload)

    @patch('cloudsearch.services.requests.requests.get')
    def test_cloudsearch_search_success(self, handle_search_mock):
        response_payload = get_valid_worker_data()
        handle_search_mock.return_value = get_response_mock(status=status.HTTP_200_OK,
                                                            content=response_payload)
        client_response_get = self.client.get(self.URL, {'search_term': get_valid_search_term()})
        client_response_post = self.client.post(self.URL, {'search_term': get_valid_search_term()})

        self.assertEqual(client_response_get.status_code, parse_status_code(status.HTTP_200_OK))
        self.assertEqual(client_response_post.status_code, parse_status_code(status.HTTP_200_OK))

        self.assertEqual(client_response_get.data, response_payload)
        self.assertEqual(client_response_post.data, response_payload)

    def test_cloudsearch_search_no_search_term(self):
        client_response_get = self.client.get(self.URL, {'search_term': ''})
        client_response_post = self.client.post(self.URL, {'search_term': ''})

        self.assertEqual(client_response_get.status_code, parse_status_code(status.HTTP_400_BAD_REQUEST))
        self.assertEqual(client_response_post.status_code, parse_status_code(status.HTTP_400_BAD_REQUEST))

        self.assertEqual(client_response_post.data.get('search_term'), 'required')
        self.assertEqual(client_response_post.data.get('search_term'), 'required')


class CloudSearchInsertTest(TestCase):
    """ Test module for CloudSearch inserts """

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.URL = '/api/cloudsearch/create/'
        cls.client = APIClient()

    def test_cloudsearch_insert_no_id(self):
        client_response_post = self.client.post(self.URL)
        self.assertEqual(client_response_post.status_code, status.HTTP_400_BAD_REQUEST)

    @patch('cloudsearch.views.cloudsearch_search')
    def test_cloudsearch_insert_worker_success(self, response_mock):
        response_mock.return_value = get_response_mock(status=status.HTTP_200_OK, content=get_valid_worker_data())
        entity_id = get_valid_worker_data()['hits']['hit'][0]['id']
        client_response_post = self.client.post(self.URL, {'id': entity_id})

        self.assertEqual(Organization.objects.count(), 0)
        self.assertEqual(HealthCareWorker.objects.count(), 1)

        worker = HealthCareWorker.objects.first()
        self.assertEqual(client_response_post.data['id'], worker.cloudsearch_id)

        worker_json = WorkerSerializer(worker).data
        worker_json['id'] = worker_json['cloudsearch_id']
        self.assertEqual(client_response_post.data, CloudSearchEntitySerializer(worker_json).data)

        self.client.post(self.URL, {'id': entity_id})
        self.assertEqual(Organization.objects.count(), 0)
        self.assertEqual(HealthCareWorker.objects.count(), 1)

    @patch('cloudsearch.views.cloudsearch_search')
    def test_cloudsearch_insert_organization_success(self, response_mock):
        response_mock.return_value = get_response_mock(status=status.HTTP_200_OK, content=get_valid_organization_data())
        entity_id = get_valid_worker_data()['hits']['hit'][0]['id']
        client_response_post = self.client.post(self.URL, {'id': entity_id})

        self.assertEqual(Organization.objects.count(), 1)
        self.assertEqual(HealthCareWorker.objects.count(), 0)

        org = Organization.objects.first()
        self.assertEqual(client_response_post.data['id'], org.cloudsearch_id)

        org_json = OrganizationSerializer(org).data
        org_json['id'] = org_json['cloudsearch_id']
        self.assertEqual(client_response_post.data, CloudSearchEntitySerializer(org_json).data)

        self.client.post(self.URL, {'id': entity_id})
        self.assertEqual(Organization.objects.count(), 1)
        self.assertEqual(HealthCareWorker.objects.count(), 0)

    @patch('cloudsearch.views.cloudsearch_search')
    def test_cloudsearch_insert_organization_fail(self, response_mock):
        response_mock.return_value = get_response_mock(status=status.HTTP_200_OK,
                                                       content=get_invalid_organization_data())
        entity_id = get_valid_worker_data()['hits']['hit'][0]['id']
        client_response_post = self.client.post(self.URL, {'id': entity_id})

        self.assertEqual(Organization.objects.count(), 0)
        self.assertEqual(HealthCareWorker.objects.count(), 0)
        self.assertEqual(client_response_post.status_code, status.HTTP_400_BAD_REQUEST)

    @patch('cloudsearch.views.cloudsearch_search')
    def test_cloudsearch_insert_worker_fail(self, response_mock):
        response_mock.return_value = get_response_mock(status=status.HTTP_200_OK,
                                                       content=get_invalid_worker_data())
        entity_id = get_valid_worker_data()['hits']['hit'][0]['id']
        client_response_post = self.client.post(self.URL, {'id': entity_id})

        self.assertEqual(Organization.objects.count(), 0)
        self.assertEqual(HealthCareWorker.objects.count(), 0)
        self.assertEqual(client_response_post.status_code, status.HTTP_400_BAD_REQUEST)

    @patch('cloudsearch.views.cloudsearch_search')
    def test_cloudsearch_insert_organization_and_worker_success(self, response_mock):
        response_mock.return_value = get_response_mock(status=status.HTTP_200_OK,
                                                       content=get_valid_organization_and_worker_data())
        entity_id = get_valid_worker_data()['hits']['hit'][0]['id']
        client_response_post = self.client.post(self.URL, {'id': entity_id})
        self.assertEqual(Organization.objects.count(), 1)
        self.assertEqual(HealthCareWorker.objects.count(), 1)

        org = Organization.objects.first()
        worker = HealthCareWorker.objects.first()
        self.assertEqual(org.workers.count(), 1)
        self.assertEqual(client_response_post.data['id'], org.cloudsearch_id)

        full_json = {**OrganizationSerializer(org).data, **WorkerSerializer(worker).data}
        full_json['id'] = full_json['cloudsearch_id']

        self.assertEqual(client_response_post.data, CloudSearchEntitySerializer(full_json).data)

        self.client.post(self.URL, {'id': entity_id})
        self.assertEqual(Organization.objects.count(), 1)
        self.assertEqual(HealthCareWorker.objects.count(), 1)

        worker.delete()
        self.assertEqual(HealthCareWorker.objects.count(), 0)
        self.client.post(self.URL, {'id': entity_id})
        self.assertEqual(Organization.objects.count(), 1)
        self.assertEqual(HealthCareWorker.objects.count(), 1)

        worker = HealthCareWorker.objects.first()
        self.assertEqual(worker.organizations.count(), 1)

        org.delete()
        self.assertEqual(Organization.objects.count(), 0)
        self.assertEqual(HealthCareWorker.objects.count(), 1)
        self.client.post(self.URL, {'id': entity_id})
        self.assertEqual(worker.organizations.count(), 1)
        self.assertEqual(Organization.objects.count(), 1)

