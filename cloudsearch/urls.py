from django.urls import path

from cloudsearch.views import search_cloudsearch, CloudSearchCreateEntityView

app_label = 'cloudsearch'
app_name = 'cloudsearch'

urlpatterns = [
    path('search/', search_cloudsearch),
    path('create/', CloudSearchCreateEntityView.as_view())
]
