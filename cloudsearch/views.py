from requests import HTTPError
from rest_framework.decorators import authentication_classes, api_view
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework import status, generics

from cloudsearch.serializers import CloudSearchEntitySerializer
from cloudsearch.services.exceptions import CloudSearchUnprocessableEntity
from cloudsearch.services.requests import cloudsearch_search
from cloudsearch.services.utils import parse_status_code, get_entities


@api_view(['GET', 'POST'])
@authentication_classes([])
def search_cloudsearch(request: Request) -> Response:
    """
    Perform search on Cloudsearch directory.
    """
    optional_params = ['start', 'size']
    mandatory_params = ['search_term']

    data = {}
    if request.method == 'POST':
        data = request.data
    elif request.method == 'GET':
        data = request.query_params

    for k in mandatory_params:
        if not data.get(k):
            return Response(data={k: 'required'}, status=status.HTTP_400_BAD_REQUEST)

    search_term = data.get('search_term')

    response = cloudsearch_search(search_term, **{k: v for k, v in data.items() if k in optional_params})
    status_code = parse_status_code(response.status_code)
    return Response(data=response.json(), status=status_code)


class CloudSearchCreateEntityView(generics.CreateAPIView):
    serializer_class = CloudSearchEntitySerializer
    permission_classes = []
    authentication_classes = []

    def post(self, request, *args, **kwargs):
        """
        Creates or updates relations between organizations and workers from CloudSearch entity.
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        entity_id = serializer.validated_data['id']
        response = cloudsearch_search(entity_id, **{'q.options': {'fields': ['ids']}})

        entity = next(get_entities(response.json()), None)
        if not entity:
            raise CloudSearchUnprocessableEntity
        serializer = self.get_serializer(data=entity)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        if serializer.data:
            return Response(data=serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        raise CloudSearchUnprocessableEntity



