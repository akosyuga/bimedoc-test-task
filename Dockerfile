FROM python:3.10.1-alpine3.14
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /app

# install psycopg2 dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

RUN pip install --upgrade pip

COPY requirements.txt /app/

# copy entrypoint.sh
COPY ./entrypoint.sh /app
RUN sed -i 's/\r$//g' /app/entrypoint.sh
RUN chmod +x /app/entrypoint.sh

RUN pip install -r requirements.txt

COPY . /app/

ENTRYPOINT ["./entrypoint.sh"]
