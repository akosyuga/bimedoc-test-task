from rest_framework import serializers

from organizations.models import Organization
from workers.serializers import WorkerSerializer


class OrganizationSerializer(serializers.ModelSerializer):
    workers = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Organization
        fields = '__all__'


class OrganizationFullSerializer(serializers.ModelSerializer):
    workers = WorkerSerializer(many=True, read_only=True)

    class Meta:
        model = Organization
        fields = '__all__'
