from django.db import models

# Create your models here.


class Organization(models.Model):
    """
    Defines a health-care organization.
    """
    activity_segment_name = models.CharField(max_length=100, blank=True)
    activity_segment_code = models.CharField(max_length=6, blank=True)
    bimedoc_client = models.BooleanField(null=True)
    building_number = models.IntegerField(null=True)
    city = models.CharField(max_length=50, blank=True)
    finess = models.CharField(max_length=50, unique=True)
    finess_justice = models.CharField(max_length=50, blank=True)
    postcode = models.CharField(max_length=50, blank=True)
    registered_name = models.CharField(max_length=50)
    cloudsearch_id = models.CharField(max_length=100, unique=True)
