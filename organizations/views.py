from rest_framework import generics
# Create your views here.
from organizations.models import Organization
from organizations.serializers import OrganizationSerializer, OrganizationFullSerializer


class OrganizationListView(generics.ListAPIView):
    serializer_class = OrganizationSerializer
    permission_classes = []
    authentication_classes = []
    queryset = Organization.objects.all()

    def get_serializer_class(self):
        if self.request.query_params.get('workers') == 'full':
            return OrganizationFullSerializer
        return OrganizationSerializer

