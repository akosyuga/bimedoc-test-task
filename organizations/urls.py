from django.urls import path

from organizations.views import OrganizationListView

app_label = 'organizations'
app_name = 'organizations'

urlpatterns = [
    path('organizations/', OrganizationListView.as_view())
]
