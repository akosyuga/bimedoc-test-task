To avoid the manual setup of the project, using Docker and Docker Compose is recommended.

---
To run the project via Docker, a Makefile can be used.

The main commands are:
- `make -f Makefile run` - creates and runs containers from compose;
- `make -f Makefile stop` - stops and removes all running containers & networks.
- `make -f Makefile run_tests` - runs all tests.

To run the project, use the first command; to stop the project, use the second one. All dependencies, migrations, etc should be handled automatically.

---

In case of manual configuration, next steps have to be done:
- venv config (dependencies in requirements.txt);
- migration via `python manage.py migrate`
- run server by `python manage.py runserver`

---

Since the task didn't require any authorization/authentication, the endpoints are universally accessible.
By default, the port is 8000, so all URLs have the same structure: `localhost:8000/api/endpoint/`.
Following endpoints are implemented:
- `GET` `api/cloudsearch/search/` - searches for an entity on CloudSearch directory by the specified `search_term`. Returns search results.   
  Query params:
  
| Name  |  Mandatory | Example  | 
|---|---|---|
|  search_term | [x] |  id |
| start  | []  |  0 |
| size  | []  | 10  |

Make sure that the query string is properly URL-encoded.

- `POST` `api/cloudsearch/search/` - same as above, but query_params have to be provided in the request body; allows providing non-escaped URL characters.

- `POST` `api/cloudsearch/create/` - creates organizations and workers from CloudSearch entity.

Request body: 

| Name  |  Mandatory | Example  | 
|---|---|---|
|  id | [x] |  PER-10000735125-F02001104501122011-020011045---21-D |

Performs search for the selected entity_id; in case of success, saves the entities, otherwise returns errors.
- `GET` `api/organizations/` - returns all saved organizations.  
If `workers=full` query parameters is present, returns full representation of related workers. Otherwise, returns PK.
  
- `GET` `api/workers/` - returns all saved workers.
---
Endpoints include default pagination via `page` parameter.
