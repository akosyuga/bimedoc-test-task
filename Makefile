run:
	docker-compose -f docker-compose.yml up -d

build:
	docker-compose -f docker-compose.yml up -d --build

stop:
	 docker-compose -f docker-compose.yml down -v --remove-orphans

restart:
	${MAKE} stop && ${MAKE} run

migrate:
	docker-compose -f docker-compose.yml exec backend python manage.py migrate --noinput

makemigrations:
	docker-compose -f docker-compose.yml exec backend python manage.py makemigrations --noinput

createsuperuser:
	docker-compose -f docker-compose.yml exec backend python manage.py createsuperuser

run_tests:
	docker-compose -f docker-compose.yml run --rm --no-deps backend python manage.py test

clean_docker:
	docker system prune -f
