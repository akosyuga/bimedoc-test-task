from rest_framework import serializers

from workers.models import HealthCareWorker


class WorkerSerializer(serializers.ModelSerializer):
    organizations = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = HealthCareWorker
        fields = '__all__'
