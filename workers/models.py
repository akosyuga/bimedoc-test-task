from django.db import models

# Create your models here.


class HealthCareWorker(models.Model):
    bimedoc_client = models.BooleanField(null=True)
    email = models.EmailField(blank=True)
    first_name = models.CharField(max_length=50)
    gender = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=50, blank=True)
    profession_code = models.CharField(max_length=50, blank=True)
    profession_name = models.CharField(max_length=50)
    rpps_number = models.CharField(max_length=20, unique=True)
    cloudsearch_id = models.CharField(max_length=100, unique=True)
    organizations = models.ManyToManyField('organizations.Organization', related_name='workers')
