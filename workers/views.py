from rest_framework import generics

from workers.models import HealthCareWorker
from workers.serializers import WorkerSerializer


class HealthCareWorkerListView(generics.ListAPIView):
    serializer_class = WorkerSerializer
    permission_classes = []
    authentication_classes = []
    queryset = HealthCareWorker.objects.all()
