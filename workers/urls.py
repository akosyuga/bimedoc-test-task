from django.urls import path

from workers.views import HealthCareWorkerListView

app_label = 'workers'
app_name = 'workers'

urlpatterns = [
    path('workers/', HealthCareWorkerListView.as_view())
]
