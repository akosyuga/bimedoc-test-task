from abc import abstractmethod

from rest_framework import serializers

from cloudsearch.services.utils import already_exists


class NestedParserSerializer(serializers.Serializer):
    """
    A class for parsing merged objects into concrete classes.
    Works by utilizing child serializers.
    """

    @staticmethod
    def _get_unique_instance(serializer):
        """
        Checks any validator for UniqueConstraint errors; in such case, retrieves the instance.

        :return: instance
        """
        unique_errors = already_exists(serializer.errors)
        if unique_errors:
            return serializer.Meta.model.objects.filter(**{k: serializer.data[k] for k in unique_errors}).first()
        return None

    @classmethod
    def create_nested_instance(cls, serializer):
        """
        A generic method for defining how new instances are handled.

        Default behaviour allows for omitting UniqueConstraint errors by fetching the appropriate instance.

        :param validated_data:
        :param serializer_class:
        :return: model instance
        """
        if serializer.is_valid():
            return serializer.save()
        else:
            return cls._get_unique_instance(serializer)

    def post_create(self, instances, **kwargs):
        """
        A post-create hook for additional operations on created instances.
        E.g. linking M2M models together.

        :param instances:
        :return:
        """
        pass

    def post_update(self, instances, **kwargs):
        """
        A post-patch hook for additional operations on created instances.
        E.g. linking M2M models together.

        :param instances:
        :return:
        """
        pass

    @abstractmethod
    def create(self, validated_data):
        self.post_create(None)
        pass

    @abstractmethod
    def update(self, instance, validated_data):
        self.post_update(None)
        pass
